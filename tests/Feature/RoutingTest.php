<?php

namespace Tests\Feature;

use Tests\TestCase;

class RoutingTest extends TestCase
{
    public function testHomePage()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    public function testBudgetRoute()
    {
        $response = $this->get('/presupuesto');
        $response->assertStatus(200);
    }

    public function testUpdateBudgetRoute()
    {
        $response = $this->post('/presupuesto');
        $response->assertStatus(200); // You can customize this assertion based on your application's logic
    }

    public function testAdminRequisitionsRoute()
    {
        $response = $this->get('/admin-requisiciones');
        $response->assertStatus(200);
    }

    public function testDashboardRoute()
    {
        $response = $this->get('/dashboard');
        $response->assertStatus(200);
    }
}

# **Proyecto No.5** – Presupuestos y requisiciones 🔥🔥

## Materia: Integración Continua de Proyectos WEB

### Profesor: Alejandro Amaro Flores

#### Integrantes del equipo

- Mario Eduardo Sánchez Mejía 21120721
- Luis Fernando Chávez Martínez 21120187
- José López Lara 19120194

##### **Requerimientos del sistema**

>
> El sistema define un presupuesto y una forma para ejercerlo, que se le denomina requisición, debe contener lo siguiente:
> 
> Usando Seeders llenar los departamentos (materiales, cómputo, escolares, etc.), los usuarios de los departamentos y el  administrador.
> 
> El administrador podrá definir el presupuesto a repa|rtir entre los departamentos (solo se hace una vez y no es > > equitativo). Cada departamento puede tener un presupuesto y no puede exceder de ese presupuesto. Además, el administrador puede aprobar o rechazar requisiciones (solicitudes de uso de ese presupuesto).
> 
> Los usuarios de los departamentos pueden hacer una requisición (registro de uso de presupuesto) definiendo el articulo o servicio, cantidad, monto unitario y subtotal de la requisición, ejemplo, paquete papel bond – cantidad: 10 – precio: 120 – total: 1200. Una vez hecha la requisición se descuenta de su presupuesto, pero tiene que ser aprobada por el administrador.
> 
> El administrador podrá aceptar requisiciones y modificar montos: Puede aumentar y/o disminuir montos y cantidades. También puede rechazar,
>
>pero tendrá que colocar el motivo y el presupuesto se regresa al departamento que realizó la requisición.
> 
> Cada usuario podrá ver su balance de presupuesto y visualizar una estadística de uso por día.
>
> El administrador, también podrá quitar presupuesto a otro departamento en caso de emergencia para asignarlo a otro
>
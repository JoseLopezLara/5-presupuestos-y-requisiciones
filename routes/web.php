<?php

use App\Livewire\Admin\Budget\AdminAsignationBudget;
use App\Livewire\Admin\Requisitions\Requisitions;
use App\Livewire\MakeRequisition;
use Illuminate\Support\Facades\Route;
use Livewire\Livewire;

Livewire::setUpdateRoute(function ($handle) {
    return Route::post('/requisiciones/livewire/update', $handle);
});

Livewire::setScriptRoute(function ($handle) {
    return Route::get('/requisiciones/livewire/livewire.js', $handle);
});
Route::get('/', function () {
    return view('welcome');
});


Route::middleware(['auth:sanctum', config('jetstream.auth_session'), 'verified',])->group(function () {
    Route::get('/presupuesto', AdminAsignationBudget::class)->name('presupuesto');
    Route::post('/presupuesto', [AdminAsignationBudget::class, 'updateBudget'])->name('updateBudget');
    Route::get('/make-requisition', MakeRequisition::class)->name('makereq');
    Route::post('/make-requisition', [MakeRequisition::class, 'store'])->name('store');
    Route::get('/admin-requisiciones', Requisitions::class)->name('admin.requisition.requests');
    Route::get('/dashboard', function () {return view('dashboard');})->name('dashboard');
});

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Requisitions extends Model
{
    use HasFactory;

    protected $table = 'requisitions';
    protected $primaryKey = 'requisition_id';
    protected $casts = [
        'items' => 'array',
    ];

    protected $fillable = [
        'n_items',
        'items',
        'total',
        'status',
        'department_id',
    ];

    public function department()
	{
		return $this->belongsTo(Departments::class, 'department_id');
	}

}

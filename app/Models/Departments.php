<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Departments extends Model
{
    use HasFactory;

    protected $table = 'departments';
    protected $primaryKey = 'department_id';

    protected $fillable = [
        'dept_name',
        'dept_budget',
        'user_id'
    ];

    //Eloquent relations
    public function requisition()
	{
		return $this->hasMany(Requisitions::class);
	}

}

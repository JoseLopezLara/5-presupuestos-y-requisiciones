<?php

namespace App\Livewire;

use App\Models\Requisitions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Livewire\Component;

class MakeRequisition extends Component
{
    public function render()
    {
        $usuario = Auth::user();
        $datos = Requisitions::where('department_id', $usuario->department_id)->paginate(10);
        return view('livewire.make-requisition',['datos' => $datos]);
    }

    public function store(Request $request)
    {
        $usuario = Auth::user();
        $requisition = new Requisitions();
        $requisition->n_items = $request->input('n_items');
        $requisition->items = $request->input('items');
        $requisition->total = $request->input('total');
        $requisition->status = "checking";
        $requisition->department_id = $usuario->department_id;
        $requisition->save();
        $datos = Requisitions::where('department_id', $usuario->department_id)->paginate(10);
        Session::flash('datos', $datos);
        return redirect()->back();
    }

}

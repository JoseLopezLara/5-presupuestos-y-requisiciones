<?php

namespace App\Livewire\Admin\Requisitions\RequisitionsRequest;

use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use App\Models\Requisitions;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;
use Livewire\Attributes\On;

class ReqisitionsRequestDataTable extends DataTableComponent
{


    protected $model = Requisitions::class;

    public function configure(): void
    {
        $this->setPrimaryKey('requisition_id');
        $this->setSingleSortingDisabled();
    }



    public function builder(): Builder
    {
        return Requisitions::query()->where('status', 'checking');
    }

    public function columns(): array
    {

        return [

            Column::make("Id", "requisition_id")
                ->format(
                    fn($value, $row, Column $column) => '<p class="inline-block bg-gray-200 py-2 px-4 rounded-md font-bold text-black">'.$value.'</p>'
                )
                ->html()->sortable()->searchable(),

            Column::make('Fecha de Solicitud')
                ->label(
                    function($row) {

                        $date = Carbon::parse($row->created_at)->format("d/m/y");
                        $time = Carbon::parse($row->created_at)->format("H:i:s");

                        return view('admin.requisitionPanel.tables.full-date', compact('date', 'time'));
                    }
                )->sortable()->searchable(),

            Column::make('Valor de la Requisición')
                ->label(
                    function($row) {

                        $n_items = $row->n_items;
                        $total = $row->total;

                        return view('admin.requisitionPanel.tables.full-price', compact('n_items', 'total'));
                    }
                )->sortable()->searchable(),

            Column::make("Departamento", "department.dept_name")
                ->format(
                    fn($value, $row, Column $column) => '<p class="inline-block text-black">'.$value.'</p>'
                )
                ->html()->sortable()->searchable(),

            // Deselected columns
            Column::make("Requisition id", "requisition_id")->sortable()->searchable()->deselected(),
            Column::make("Created at", "created_at")->sortable()->searchable()->deselected(),
            Column::make("Quantity", "n_items")->sortable()->searchable()->deselected(),
            Column::make("Total", "total")->sortable()->searchable()->deselected(),
            Column::make('Acciones')
            ->label(
                fn ($row, Column $column) => view('admin.requisitionPanel.tables.actions-column')->with(
                    [
                        'requisition_id' => $row->requisition_id,
                    ]
                )
            )->html(),
        ];

    }
}

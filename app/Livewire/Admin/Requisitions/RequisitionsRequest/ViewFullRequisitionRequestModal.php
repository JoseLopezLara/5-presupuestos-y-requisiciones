<?php

namespace App\Livewire\Admin\Requisitions\RequisitionsRequest;

use App\Models\Departments;
use App\Models\Requisitions;
use Livewire\Component;
use Livewire\Attributes\On;
use App\Models\User;
use Carbon\Carbon;

class ViewFullRequisitionRequestModal extends Component
{

    //Models
    public $requisitionRequest, $department;
    //Requisition Request
    public $requisition_id, $item, $quantity, $unit_price, $subtotal, $status,
    $department_id, $create_date, $create_time, $update_date, $update_time;

    public $n_items, $total, $items = [];

    //Department
    public $dept_name, $dept_budget, $dept_budget_if_accept_requisition;

    //Confirmation
    public $confirmAcceptRequisition = false;
    public $confirmDenegateRequisition = false;
    public $budgetIsEnough = false;

    //TODO: Test
    //public $quantityInputs = [];


    public function render()
    {
        return view('admin.requisitionPanel.modals.view-full-requisition-request-modal');
    }

    #[On('loadRequisitionRequest')]
    public function loadRequest($requisition_id)
    {
        $this->requisitionRequest = Requisitions::find($requisition_id);

        if($this->requisitionRequest){
            $this->requisition_id = $this->requisitionRequest->requisition_id;

            $this->n_items = $this->requisitionRequest->n_items;
            $this->items = json_decode($this->requisitionRequest->items, true);
            $this->total = $this->requisitionRequest->total;

            $this->status = $this->requisitionRequest->status;
            $this->department_id = $this->requisitionRequest->department_id;

            $this->create_date = Carbon::parse($this->requisitionRequest->created_at)->format("d/m/y");
            $this->create_time = Carbon::parse($this->requisitionRequest->created_at)->format("H:i:s");
            $this->update_date = Carbon::parse($this->requisitionRequest->updated_at)->format("d/m/y");
            $this->update_time = Carbon::parse($this->requisitionRequest->updated_at)->format("H:i:s");

            $this->department = Departments::find($this->department_id);
            $this->dept_name = $this->department->dept_name;
            $this->dept_budget = $this->department->dept_budget;
            $this->dept_budget_if_accept_requisition = $this->department->dept_budget - $this->total;
            $this->mountInputs();

            $this->checkIfTheBudgetIsEnough();

        }
    }

    function mountInputs(){

    }

    public function checkIfTheBudgetIsEnough(){
        $this->budgetIsEnough = ($this->dept_budget_if_accept_requisition >= 0) ? true : false;

    }

    public function performAcceptRequisition(){
        try{

            //TODO: Add validate to values
            $this->validate([
                'dept_budget_if_accept_requisition' => 'required|numeric|gt:0',
                'items.*.item' => 'required|string|max:255',
                'items.*.quantity' => 'required|numeric|min:0|max:99',
                'items.*.unit_price' => 'required|numeric|min:0',
                'items.*.subtotal' => 'required|numeric|min:0',
            ]);

            //Update status
            $this->requisitionRequest->update([
                'status' => 'accepted',
                'items' => json_encode($this->items),
                'total' => $this->total
            ]);

            //Update budget
            $this->department->update([
                'dept_budget' => $this->dept_budget_if_accept_requisition
            ]);

            $this->dispatch('refreshDatatable');
            $this->confirmAcceptRequisition = false;
            $this->dispatch('close-modal', 'viewFullRequisitionRequestModal');
            session()->flash('flash.banner', 'Requisición aceptada correctamente');

        }catch(\Exception $e){
            $this->confirmAcceptRequisition = false;
            info($e->getMessage());
            $this->addError('Materia', 'Error El presupuesto no es suficiente o las cantides y montos son negativas');
            return null;
        }
    }

    public function performDenegateRequisition(){
        try{
            $this->requisitionRequest->update([
                'status' => 'denegated'
            ]);

            $this->dispatch('refreshDatatable');
            $this->confirmDenegateRequisition = false;
            $this->dispatch('close-modal', 'viewFullRequisitionRequestModal');
            session()->flash('flash.banner', 'Requisición rechazada correctamente');

        }catch(\Exception $e){
            $this->notification()->error(
                $title = 'Hubo un error',
                $description = 'Ha ocurrido un error al rechazar'
            );
        }
    }


    //Update any value (Quantity or unit price) of a Item value of a requsition.
    public function updateItemValue($newValue ,$indexInput){


        try{
            $this->validate([
                'items.*.quantity' => 'required|numeric|min:0|max:99',
                'items.*.unit_price' => 'required|numeric|min:0',
            ]);

            $this->items[$indexInput]['subtotal'] = $this->items[$indexInput]['quantity'] * $this->items[$indexInput]['unit_price'];

            //Update total requisition value
            $totalUpdated = 0;
            foreach($this->items as $index => $item){
                $totalUpdated += $this->items[$index]['subtotal'];
            }

            //Update total
            $this->total = $totalUpdated;

            //Update new budget next to accept requisition
            $this->dept_budget_if_accept_requisition = $this->department->dept_budget - $this->total;

            //Verify if new budget is enough
            $this->checkIfTheBudgetIsEnough();

        }catch(\Exception $e){
            info($e->getMessage());
            $this->addError('Materia', 'No puede dejar campos vacios o ingresar letras');
            return null;
        }
    }

    public function updated(){


        try{
            $this->validate([
                'items.*.quantity' => 'required|numeric|min:0|max:99',
                'items.*.unit_price' => 'required|numeric|min:0',
            ]);


        }catch(\Exception $e){
            info($e->getMessage());
            $this->addError('Materia', 'No puede dejar campos vacios o ingresar letras');
            return null;
        }
    }


}

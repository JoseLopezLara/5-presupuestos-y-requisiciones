<?php

namespace App\Livewire\Admin\Requisitions;

use Livewire\Component;

class Requisitions extends Component
{
    public function render()
    {
        return view('livewire.admin.requisitions.requisitions');
    }
}

<?php

namespace App\Livewire\Admin\Requisitions;

use Livewire\Attributes\On;
use Livewire\Component;

class AdminRequisitionPanel extends Component
{
    public $showReqisitionsRequestDataTable = true;
    public $showReqisitionsAcceptedDataTable = false;

    public function render()
    {

        return view('livewire.admin.requisitionPanel.admin-requisition-panel');
    }


    #[On('chanceTypeToggleRequisition')]
    public function chanceTypeToggleRequisition($type) {
        $this->showReqisitionsRequestDataTable = !$this->showReqisitionsRequestDataTable;
        $this->showReqisitionsAcceptedDataTable = !$this->showReqisitionsAcceptedDataTable;
    }

}

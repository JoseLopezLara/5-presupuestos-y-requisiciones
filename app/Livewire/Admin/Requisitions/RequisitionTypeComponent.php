<?php

namespace App\Livewire\Admin\Requisitions;

use Livewire\Component;

class RequisitionTypeComponent extends Component
{
    public $monuntOptions = [];
    public $showRequisitionsType = 1;

    public function render()
    {
        return view('livewire.admin.requisitionPanel.requisition-type-component');
    }

    public function mount(){
        $this->mountRequestTypeSelector();
    }

    public function mountRequestTypeSelector(){
        array_push($this->monuntOptions, [
            "idOption" => 1,
            "type" => "En revisión"
        ]);
        array_push($this->monuntOptions, [
            "idOption" => 2,
            "type" => "Aceptadas"
        ]);
        ;
    }

    public function updated(){
        $this->dispatch(
            'chanceTypeToggleRequisition',
            ($this->showRequisitionsType == 1) ? 'checking' : 'accepted'
        );
    }
}

<?php

namespace App\Livewire\Admin\Requisitions\RequisitionsAccepted;

use App\Models\Departments;
use App\Models\Requisitions;
use Livewire\Component;
use Livewire\Attributes\On;
use App\Models\User;
use Carbon\Carbon;

class ViewFullRequisitionAcceptedModal extends Component
{

    //Models
    public $requisitionRequest, $department;
    //Requisition Request
    public $requisition_id, $item, $quantity, $unit_price, $subtotal, $status,
    $department_id, $create_date, $create_time, $update_date, $update_time, $motiveReturnRequisition;

    public $n_items, $total, $items;

    //Department
    public $dept_name, $dept_budget, $dept_budget_if_return_requisition;

    //Confirmation
    public $confirmReturnRequisition = false;

    public function render()
    {
        return view('admin.requisitionPanel.modals.view-full-requisition-accepted-modal');
    }

    #[On('loadRequisitionRequestCancel')]
    public function loadRequest($requisition_id)
    {
        $this->requisitionRequest = Requisitions::find($requisition_id);

        if($this->requisitionRequest){
            $this->requisition_id = $this->requisitionRequest->requisition_id;

            $this->n_items = $this->requisitionRequest->n_items;
            $this->items = json_decode($this->requisitionRequest->items, true);
            $this->total = $this->requisitionRequest->total;

            $this->status = $this->requisitionRequest->status;
            $this->department_id = $this->requisitionRequest->department_id;

            $this->create_date = Carbon::parse($this->requisitionRequest->created_at)->format("d/m/y");
            $this->create_time = Carbon::parse($this->requisitionRequest->created_at)->format("H:i:s");
            $this->update_date = Carbon::parse($this->requisitionRequest->updated_at)->format("d/m/y");
            $this->update_time = Carbon::parse($this->requisitionRequest->updated_at)->format("H:i:s");

            $this->department = Departments::find($this->department_id);
            $this->dept_name = $this->department->dept_name;
            $this->dept_budget = $this->department->dept_budget;
            $this->dept_budget_if_return_requisition = $this->department->dept_budget + $this->total;
        }
    }

    public function toggle()
    {
        dd("entro");
    }


    public function performCreateReturnRequisition(){
        try{

            $this->validate([
                'status' => 'required',
                'motiveReturnRequisition' => 'required|min:3|max:255',
            ]);

            //Update status
            $this->requisitionRequest->update([
                'status' => 'denegated',
                'motive_return_requisition' => $this->motiveReturnRequisition
            ]);

            //Update budget
            $this->department->update([
                'dept_budget' => $this->dept_budget_if_return_requisition
            ]);

            $this->dispatch('refreshDatatable');
            $this->confirmReturnRequisition = false;
            $this->dispatch('close-modal', 'viewFullRequisitionAcceptedModal');
            session()->flash('flash.banner', 'Requisición retornada correctamente');

        }catch(\Exception $e){
            // dd($e);
            // $this->notification()->error(
            //     $title = 'Hubo un error',
            //     $description = 'Ha ocurrido un error al retornar'
            // );


            //session()->flash('flash.banner', 'Campos requeridos');
            $this->confirmReturnRequisition = false;
            //$this->dispatch('close-modal', 'viewFullRequisitionAcceptedModal');
            //session()->flash('flash.banner', 'Campos requeridos');
            info($e->getMessage());
            $this->addError('Materia', 'Campo requerido | Minimo 3 Caracteres | Maximo 255 Caracteres');
            return null;

        }
    }

}

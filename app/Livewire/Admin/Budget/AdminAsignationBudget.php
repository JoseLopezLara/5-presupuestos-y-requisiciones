<?php

namespace App\Livewire\Admin\Budget;

use App\Models\Departments;
use Livewire\Component;

class AdminAsignationBudget extends Component
{
    public $opcion;
    public $opcion2;
    public $presupuesto1;
    public $opcion3;
    public $presupuesto;
    public $presupuesto2; // Nuevo
    public $datosVacios2=false;
    public $budgetVacios=false;
    public $actualizacionExitosa = false;
    public $problemaActualizacion = false;
    public $problemaActualizacion2 = false;
    public $datosVacios = false;
    public $valoresIncorrectos = false;


    public function render()
    {
        $departments = Departments::get(['department_id', 'dept_name', 'dept_budget']);
        return view('livewire.admin.budget.admin-asignation-budget', compact('departments'));
    }

    public function buscarPresupuesto2()
    {
        if (!empty($this->opcion2)) {
            $department = Departments::find($this->opcion2);
            if ($department && $department->dept_budget != 0) {
                $this->presupuesto2 = $department->dept_budget;
            }else{
                $this->budgetVacios = true;
            }
        } else {
            $this->datosVacios2 = true;
            $this->presupuesto2 = 0;
        }
    }

    public function trasferirPresupuesto()
    {
        if ($this->opcion2 == "" || $this->opcion3 == "" || $this->presupuesto1 == "") {
            $this->datosVacios = true;
            return;
        }

        if ($this->presupuesto1 <= 0) {
            $this->valoresIncorrectos = true;
            return;
        }

        $department1 = Departments::find($this->opcion2);
        $department2 = Departments::find($this->opcion3);

        if ($department1->dept_budget < $this->presupuesto1) {
            $this->problemaActualizacion2 = true;
            return;
        }


        // Actualizar el presupuesto
        $department1->dept_budget -= $this->presupuesto1;
        $department2->dept_budget += $this->presupuesto1;
        $department1->save();
        $department2->save();
        $this->actualizacionExitosa = true;
        $this->borrarValores();
        //$this->buscarPresupuesto2(); // Llama a la función para buscar el presupuesto2
    }

    public function updateBudget()
    {
        $this->resetValues();
        if ($this->opcion == "" || $this->presupuesto == "") {
            $this->datosVacios = true;
            return;
        }

        if ($this->presupuesto <= 0) {
            $this->valoresIncorrectos = true;
            return;
        }

        $department = Departments::find($this->opcion);

        if ($department->dept_budget != 0) {
            $this->problemaActualizacion = true;
            return;
        }

        // Actualizar el presupuesto
        $department->dept_budget = $this->presupuesto;
        $department->save();
        $this->actualizacionExitosa = true;



        //$this->buscarPresupuesto2(); // Llama a la función para buscar el presupuesto2
        $this->borrarValores();
    }

    public function resetValues()
    {
        $this->actualizacionExitosa = false;
        $this->problemaActualizacion = false;
        $this->datosVacios = false;
        $this->valoresIncorrectos = false;
        $this->datosVacios2 = false;
        $this->budgetVacios = false;
        $this->problemaActualizacion2 = false;
    }

    public function borrarValores()
    {
        $this->opcion = "";
        $this->opcion2 = "";
        $this->opcion3 = "";
        $this->presupuesto = "";
        $this->presupuesto1 = "";
        $this->presupuesto2 = "";
    }

    public function setOption2($value)
    {
        $this->opcion2 = $value;
        $this->buscarPresupuesto2();
    }
}

<?php

namespace App\Actions\Fortify;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Laravel\Jetstream\Jetstream;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array<string, string>  $input
     */
    public function create(array $input): User
    {
        // Validator::make($input, [
        //     'name' => ['required', 'string', 'max:255'],
        //     'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        //     'password' => $this->passwordRules(),
        //     'terms' => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['accepted', 'required'] : '',
        // ])->validate();

        // return User::create([
        //     'name' => $input['name'],
        //     'email' => $input['email'],
        //     'password' => Hash::make($input['password']),
        // ]);

        Validator::make($input, [
            'user_name' => ['required', 'string', 'max:255'],
            'user_paternal_surname' => ['required', 'string', 'max:255'],
            'user_maternal_surname' => ['required', 'string', 'max:255'],
            'department' => ['required', 'numeric', 'between:1,10'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
            'terms' => Jetstream::hasTermsAndPrivacyPolicyFeature() ? ['accepted', 'required'] : '',
        ])->validate();

        //dd($input);
        return User::create([
            'user_name' => $input['user_name'],
            'user_paternal_surname' => $input['user_paternal_surname'],
            'user_maternal_surname' => $input['user_maternal_surname'],
            'department_id' => $input['department'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);
    }
}

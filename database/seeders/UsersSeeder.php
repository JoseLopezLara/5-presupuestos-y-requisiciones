<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    public function run()
    {
        $roles = [1, 2, 3]; // IDs de los 3 roles

        foreach ($roles as $roleId) {
            for ($i = 1; $i <= 20; $i++) {
                $user = new User();
                $user->user_name = "User_" . $i . "_Role_" . $roleId;
                $user->user_paternal_surname = "Pat" . $i . "_Role_" . $roleId;
                $user->user_maternal_surname = "Mat" . $i . "_Role_" . $roleId;
                $user->email = "user" . $i . "_role_" . $roleId . "@example.com";
                $user->password = bcrypt('password');
                //TODO: Check if need delete after spatie instalation
                //$user->role_id = $roleId;
                if($roles == 1){
                   $user->assignRole('SuperUser');
                }
                if($roles == 2){
                    $user->assignRole('Administrator');
                }
                if($roles == 3){
                $user->assignRole('Employee');
                }

                $user->department_id = rand(1, 10);

                $user->save();
            }
        }
    }
}



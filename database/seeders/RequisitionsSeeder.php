<?php

namespace Database\Seeders;

use App\Models\Requisitions;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RequisitionsSeeder extends Seeder
{
    public function run(): void
    {
        $totalRecords = 5;

        for ($i = 0; $i < $totalRecords; $i++) {
            $itemsCount = rand(3, 8);
            $items = [];

            for ($j = 0; $j < $itemsCount; $j++) {
                $item = "Item " . rand(1, 100);
                $quantity = rand(1, 10);
                $unitPrice = rand(100, 1000);
                $subtotal = $quantity * $unitPrice;

                $items[] = [
                    'item' => $item,
                    'quantity' => $quantity,
                    'unit_price' => $unitPrice,
                    'subtotal' => $subtotal,
                ];
            }

            $requisition = new Requisitions();
            $requisition->n_items = $itemsCount;
            $requisition->items = json_encode($items);
            $requisition->total = array_sum(array_column($items, 'subtotal'));
            $requisition->status = 'checking';
            $requisition->department_id = 1;
            $requisition->save();
        }
    }
}

<?php

namespace Database\Seeders;

use App\Models\Departments;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DepartmentsSeeder extends Seeder
{
    public function run(): void
    {
        $department1 = new Departments();
        $department1->dept_name = "Sistemas";
        $department1->dept_budget = 0;
        $department1->save();

        $department2 = new Departments();
        $department2->dept_name = "Ventas";
        $department2->dept_budget = 0;
        $department2->save();

        $department3 = new Departments();
        $department3->dept_name = "Marketing";
        $department3->dept_budget = 0;
        $department3->save();

        $department4 = new Departments();
        $department4->dept_name = "Recursos Humanos";
        $department4->dept_budget = 0;
        $department4->save();

        $department5 = new Departments();
        $department5->dept_name = "Producción";
        $department5->dept_budget = 0;
        $department5->save();

        $department6 = new Departments();
        $department6->dept_name = "Finanzas";
        $department6->dept_budget = 0;
        $department6->save();

        $department7 = new Departments();
        $department7->dept_name = "Logística";
        $department7->dept_budget = 0;
        $department7->save();

        $department8 = new Departments();
        $department8->dept_name = "Calidad";
        $department8->dept_budget = 0;
        $department8->save();

        $department9 = new Departments();
        $department9->dept_name = "Investigación y Desarrollo";
        $department9->dept_budget = 0;
        $department9->save();

        $department10 = new Departments();
        $department10->dept_name = "Soporte Técnico";
        $department10->dept_budget = 0;
        $department10->save();

    }
}

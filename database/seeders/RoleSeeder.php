<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class RoleSeeder extends Seeder
{
    public function run(): void
    {

        //description = "The SuperUser role has full access to all system functionalities and can manage other users and settings.";
        $role1 = Role::create(['name' => 'SuperUser']);

        //description = "Administrators have permissions to configure system settings, manage user roles, and oversee system operations.";
        $role2 = Role::create(['name' => 'Administrator']);

        //description = "Employees have access to specific modules based on their roles and responsibilities within the organization.";
        $role3 = Role::create(['name' => 'Employee']);


        //-------------- TODO: Add permision, HERE --------------
        //-------------------------------------------------------
        $permissionAdminDashboard = Permission::create(['name' => 'admin.dashboard']);


        //-------------- TODO: Add relations, HERE --------------
        //-------------------------------------------------------
        //Relation between permissions and role
        $permissionAdminDashboard->syncRoles([$role1, $role2]);

    }
}

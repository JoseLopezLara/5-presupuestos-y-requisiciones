<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('requisitions', function (Blueprint $table) {
            $table->id('requisition_id');
            $table->integer('n_items');
            $table->json('items');
            $table->double('total');
            $table->string('motive_return_requisition')->nullable();
            $table->enum('status', ['checking', 'accepted', 'denegated'])->nullable();
            $table->timestamps();

            $table->unsignedBigInteger('department_id');

            $table->foreign('department_id')->references('department_id')->on('departments');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('requisitions');
    }
};

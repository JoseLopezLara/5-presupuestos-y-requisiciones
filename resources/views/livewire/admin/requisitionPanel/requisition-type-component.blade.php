<div class="ml-1 text-black">
    <label for="opcion" class="ml-2 my-2 block text-gray-800 text-xl font-bold ">Tipo de requisicion</label>
    <select
        id="opcion"
        wire:click="$refresh"
        name="opcion"
        wire:model="showRequisitionsType"
        class=" shadow-sm appearance-none rounded-lg border-2  border-blue-700 w-1/4 my-2 mx-1 py-3 px-3.5  text-gray-500 leading-tight focus:outline-none focus:shadow-outline">

            @foreach($monuntOptions as $monuntOption)
                <option  value="{{ $monuntOption["idOption"] }}">{{ $monuntOption["type"] }}</option>
            @endforeach
    </select>
</div>

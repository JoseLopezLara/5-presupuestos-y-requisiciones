<div>
    <div class="p-10 mt-5 mx-5 rounded-xl shadow-sm bg-white">
        @livewire('Admin.Requisitions.RequisitionTypeComponent')
    </div>

    <div class="p-10 my-5  mx-5 rounded-xl shadow-sm bg-white">

        @if ($showReqisitionsRequestDataTable)
            @livewire('Admin.Requisitions.RequisitionsRequest.ReqisitionsRequestDataTable')
            @livewire('Admin.Requisitions.RequisitionsRequest.ViewFullRequisitionRequestModal')
        @endif

        @if ($showReqisitionsAcceptedDataTable)
            @livewire('Admin.Requisitions.RequisitionsAccepted.ReqisitionsAcceptedDataTable')
            @livewire('Admin.Requisitions.RequisitionsAccepted.ViewFullRequisitionAcceptedModal')
        @endif
    </div>
</div>

<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Panel de Requisiciones') }}
        </h2>
    </x-slot>

    <x-slot name="slot">
        @livewire('Admin.Requisitions.AdminRequisitionPanel')
    </x-slot>
</div>

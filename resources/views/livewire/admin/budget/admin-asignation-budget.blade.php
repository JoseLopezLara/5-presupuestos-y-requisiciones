<div class="flex justify-center mt-16 space-x-8">
    <div class="grid bg-white rounded-2xl shadow-2xl p-8">
        <div class="grid space-y-4">
            <label class="text-black">Departamento</label>
            <select wire:model="opcion" class="form-control">
                <option value="">Seleccione un departamento</option>
                @foreach($departments as $department)
                    <option value="{{ $department->department_id }}">{{ $department->dept_name }}</option>
                @endforeach
            </select>
            <x-input wire:model="presupuesto" label="Presupuesto" type="number" />
            <button wire:click="updateBudget" class="btn btn-success ml-4">Guardar</button>
        </div>
        <div class="divider divider-neutral text-black">Transferir Presupuesto</div>
        <div class="h-2/3 space-y-4">
            <div class="flex space-x-4">
                <select wire:model="opcion2" class="form-control">
                    <option value="">Seleccione el departamento 1</option>
                    @foreach($departments as $department)
                        <option value="{{ $department->department_id }}">{{ $department->dept_name }}</option>
                    @endforeach
                </select>
                <button wire:click="buscarPresupuesto2" class="btn btn-accent">Check</button>
            </div>
            <p class="text-black">Presupuesto del Departamento: {{$presupuesto2}}</p>
            <x-input wire:model="presupuesto1" label="Presupuesto a Transferir" type="number" />
            <div class="flex">
                <select wire:model="opcion3" class="form-control">
                    <option value="">Seleccione el departamento 2</option>
                    @foreach($departments as $department)
                        @if($department->department_id != $opcion2)
                            <option value="{{ $department->department_id }}">{{ $department->dept_name }}</option>
                        @endif
                    @endforeach
                </select>
                <button wire:click="trasferirPresupuesto" class="btn btn-outline btn-success ml-4">Transferir</button>
            </div>

        </div>
    </div>

    @if($actualizacionExitosa)
        <div class="toast">
            <div class="alert alert-success">
                <span>Actualización éxitosa</span>
                <button wire:click="resetValues">x</button>
            </div>
        </div>
    @endif

    @if($problemaActualizacion)
        <div class="toast">
            <div class="alert bg-red-600 text-white">
                <span>El departamento al que quiere asignar ya tiene presupuesto</span>
                <button wire:click="resetValues">x</button>
            </div>
        </div>
    @endif

    @if($problemaActualizacion2)
        <div class="toast">
            <div class="alert bg-red-600 text-white">
                <span>El presupuesto a transferir es mayor al presupuesto del departamento</span>
                <button wire:click="resetValues">x</button>
            </div>
        </div>
    @endif

    @if($datosVacios)
        <div class="toast">
            <div class="alert bg-red-600 text-white">
                <span>Llene todos los campos</span>
                <button wire:click="resetValues">x</button>
            </div>
        </div>
    @endif

    @if($valoresIncorrectos)
        <div class="toast">
            <div class="alert bg-amber-600 text-white">
                <span>No son válidos los valores menor o iguales a 0</span>
                <button wire:click="resetValues">x</button>
            </div>
        </div>
    @endif

    @if($datosVacios2)
        <div class="toast">
            <div class="alert bg-red-600 text-white">
                <span>Seleccione un departamento antes de hacer esta opción</span>
                <button wire:click="resetValues">x</button>
            </div>
        </div>
    @endif

    @if($budgetVacios)
        <div class="toast">
            <div class="alert bg-amber-600 text-white">
                <span>El presupuesto del departamento es 0</span>
                <button wire:click="resetValues">x</button>
            </div>
        </div>
    @endif

    <div class="bg-white rounded-2xl p-8 shadow-2xl">
        <div class="overflow-x-auto">
            <table class="table">
                <thead class="text-black">
                <tr>
                    <th>Nombre Departamento</th>
                    <th>Presupuesto</th>
                </tr>
                </thead>
                <tbody>
                @foreach($departments as $department)
                    <tr>
                        <td>{{$department->dept_name}}</td>
                        <td>{{$department->dept_budget}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Crear Requisición
        </h2>
    </x-slot>
    <dialog id="error" class="modal">
        <div class="modal-box bg-white text-black">
            <h3 class="font-bold text-lg">Error al agregar</h3>
            <p>Rellena los campos correctamente para agregar</p>
            <div class="modal-action">
            <form method="dialog">
                <button class="btn">Cerrar</button>
            </form>
            </div>
        </div>
    </dialog>
    <dialog id="tablavacia" class="modal">
        <div class="modal-box bg-white text-black">
            <h3 class="font-bold text-lg">Lista Vacia</h3>
            <p>Para enviar la solicitud al menos agrega un item a tu lista.</p>
            <div class="modal-action">
            <form method="dialog">
                <button class="btn">Cerrar</button>
            </form>
            </div>
        </div>
    </dialog>
    <dialog id="enviar" class="modal">
        <div class="modal-box bg-white text-black">
            <h3 class="font-bold text-lg">Confirmar Envió</h3>
            <p>¿Deseas enviar el contenido actual?.</p>
            <div class="modal-action">
            <form method="dialog">
                <button class="btn">Cancelar</button>
                <button class="btn" onclick="sendList()">Confirmar</button>
            </form>
            </div>
        </div>
    </dialog>
    <div class="bg-white flex flex-col items-start md:grid md:grid-cols-2 px-6 lg:px-8 mt-4 rounded-2xl mx-4">
        <div class="p-10 text-black text-start flex flex-col space-y-2">
            <label for="name">Nombre del Producto:</label>
            <input type="text" id="name" placeholder="Nombre" class="w-full p-2 border border-gray-300 rounded-l">
            <label for="cantidad">Cantidad de Productos:</label>
            <input type="number" id="cantidad" placeholder="Cantidad" min="1" class="w-full p-2 border border-gray-300 rounded-l">
            <label for="price">Precio del Producto:</label>
            <input type="number" id="price" placeholder="Precio $" min="0" step="0.01" class="w-full p-2 border border-gray-300 rounded-l">
            <button class="addBtn bg-indigo-800 text-white text-center p-2 rounded-r cursor-pointer transition duration-300 hover:bg-indigo-400" onclick="newElement()">Agregar</button>
            <div class="flex justify-between items-center">
                <button  class="mt-4 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded w-2/4" onclick="validateAndSend()">Enviar Lista</button>
                <div id="totalContainer" class="text-black text-xl text-end mt-4">Total: <span id="totalValue">0</span></div>
            </div>
        </div>
        <div>
            <table id="myTable" class="w-full mt-10 text-white border-collapse">
                <thead>
                    <tr>
                        <th class="border px-4 py-2 bg-blue-400">Nombre</th>
                        <th class="border px-4 py-2 bg-blue-400">Cantidad</th>
                        <th class="border px-4 py-2 bg-blue-400">Precio</th>
                        <th class="border px-4 py-2 bg-blue-400">Subtotal</th>
                        <th class="border px-4 py-2 bg-blue-400">Acciones</th>
                    </tr>
                </thead>
                <tbody id="myTableBody"></tbody>
            </table>
        </div>
        <div class="hidden">
            <form id="dataForm" wire:submit="store" method="POST">
                @csrf
                <input type="hidden" wire:model="n_items" name="n_items" id="n_items">
                <input type="hidden" wire:model="items" name="items" id="items">
                <input type="hidden" wire:model="total" name="total" id="total">
            </form>
        </div>
    </div>

    <div class="bg-gray-200 bg-opacity-25 py-8">
        <div class="mx-4">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <table class="min-w-full divide-y divide-gray-200">
                        <thead class="bg-gray-50">
                            <tr>
                                <th scope="col" class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider w-1/12">
                                    No. Productos
                                </th>
                                <th scope="col" class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider w-2/12">
                                    Productos
                                </th>
                                <th scope="col" class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider w-2/12">
                                    Total
                                </th>
                                <th scope="col" class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider w-2/12">
                                    Estatus
                                </th>
                                <th scope="col" class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider w-5/12">
                                    Motivo de Rechazo
                                </th>
                            </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200 text-black">
                            @foreach ($datos as $dato)
                            <tr>
                                <td class="px-6 text-center py-4 whitespace-nowrap">{{ $dato->n_items }}</td>
                                <td class="px-6 text-center py-4 whitespace-nowrap">
                                    <button class="btn bg-blue-500 text-white" id="m{{ $dato->requisition_id }}" onclick="document.getElementById('itemsM{{ $dato->requisition_id }}').showModal()">Lista Productos</button>
                                    <dialog id="itemsM{{ $dato->requisition_id }}" class="modal">
                                        <div class="modal-box w-11/12 max-w-5xl bg-white py-8">
                                            <h3 class="font-bold text-2xl py-4">Productos de la Requisición</h3>
                                            <div class="overflow-x-auto" style="max-height: 400px; overflow-y: auto;">
                                                <table class="table">
                                                    <thead>
                                                        <tr class="text-xl">
                                                            <th class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider w-6/12">Nombre</th>
                                                            <th class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider w-2/12">Cantidad</th>
                                                            <th class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider w-2/12">Precio Unitario</th>
                                                            <th class="px-6 py-3 text-center text-xs font-medium text-gray-500 uppercase tracking-wider w-2/12">SubTotal</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                        $items = is_string($dato->items) ? json_decode($dato->items) : $dato->items;
                                                        @endphp
                                                        @foreach ($items as $item)
                                                        <tr>
                                                            <td class="px-6 py-4 text-left whitespace-nowrap">{{ $item->item }}</td>
                                                            <td class="px-6 py-4 text-center whitespace-nowrap">{{ $item->quantity }}</td>
                                                            <td class="px-6 py-4 text-center whitespace-nowrap">
                                                                @php
                                                                $unitprice = number_format($item->unit_price, 2, '.', ',');
                                                                @endphp
                                                                {{ $unitprice }}
                                                            </td>
                                                            <td class="px-6 py-4 text-center whitespace-nowrap">
                                                                @php
                                                                $subtotal = number_format($item->subtotal, 2, '.', ',');
                                                                @endphp
                                                                {{ $subtotal }}
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <form method="dialog" class="modal-backdrop">
                                            <button>Close</button>
                                        </form>
                                    </dialog>
                                </td>
                                <td class="px-6 py-4 text-center whitespace-nowrap">
                                    @php
                                    $total = number_format($dato->total, 2, '.', ',');
                                    @endphp
                                    {{ $total }}
                                </td>
                                <td class="px-6 text-center py-4 whitespace-nowrap">{{ $dato->status }}</td>
                                <td class="px-6 text-center py-4 whitespace-nowrap">{{ $dato->motive_return_requisition }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="mt-4">
                        {{ $datos->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function sendList() {
        var data = [];
        var rows = document.getElementById("myTableBody").rows;
        var nitems = 0;
        var nnitems = 0;
        var total = 0;
        for (var i = 0; i < rows.length; i++) {
            var rowData = {
                item: rows[i].cells[0].textContent,
                quantity: rows[i].cells[1].textContent,
                unit_price: parseFloat(rows[i].cells[2].textContent.replace(/[^0-9.-]+/g,"")),
                subtotal: parseFloat(rows[i].cells[3].textContent.replace(/[^0-9.-]+/g,""))
            };
            nitems++;
            nnitems += parseFloat(rows[i].cells[1].textContent);
            total += parseFloat(rows[i].cells[3].textContent.replace(/[^0-9.-]+/g,""));
            data.push(rowData);
        }
        document.getElementById("n_items").value = JSON.stringify(nitems);
        document.getElementById("items").value = JSON.stringify(data);
        document.getElementById("total").value = JSON.stringify(total);
        document.getElementById("dataForm").submit();
        clearTable();
    }

    function validateAndSend() {
        var totalItems = document.getElementById("myTableBody").rows.length;
        if (totalItems === 0) {
            tablavacia.showModal();
            return;
        }
        var totalValue = document.getElementById("totalValue").innerText;

        var totalPrecios = parseFloat(totalValue.replace(/[^0-9.-]+/g,""));
        var cantidadActual = 0;
        var rows = document.getElementById("myTableBody").rows;
        for (var i = 0; i < rows.length; i++) {
            cantidadActual += parseInt(rows[i].cells[1].textContent);
        }
        enviar.showModal();
    }

    function newElement() {
        var nameValue = document.getElementById("name").value;
        var cantidadValue = document.getElementById("cantidad").value;
        var priceValue = document.getElementById("price").value;

        var errors = [];

        if (nameValue === '') {
            errors.push("Nombre es un campo obligatorio.");
        }
        if (cantidadValue <= 0 || isNaN(cantidadValue)) {
            errors.push("Cantidad debe ser mayor que 0.");
        }
        if (priceValue <= 0 || isNaN(priceValue)) {
            errors.push("Precio debe ser mayor que 0.");
        }

        if (errors.length > 0) {
            error.showModal()
            return;
        }

        var tableBody = document.getElementById("myTableBody");

        var newRow = tableBody.insertRow();
        newRow.classList.add("alternate-row");

        var cellName = newRow.insertCell(0);
        cellName.classList.add("border", "px-4", "py-2", "text-center", "text-black");

        var cellCantidad = newRow.insertCell(1);
        cellCantidad.classList.add("border", "px-4", "py-2", "text-center", "text-black");

        var cellPrice = newRow.insertCell(2);
        cellPrice.classList.add("border", "px-4", "py-2", "text-center", "text-black");

        var cellSubtotal = newRow.insertCell(3);
        cellSubtotal.classList.add("border", "px-4", "py-2", "text-center", "text-black");

        var cellActions = newRow.insertCell(4);
        cellActions.classList.add("border", "px-4", "py-2", "text-center", "text-black");

        var subtotal = parseFloat(cantidadValue) * parseFloat(priceValue);

        cellName.textContent = nameValue;
        cellCantidad.textContent = cantidadValue;
        cellPrice.textContent = parseFloat(priceValue).toLocaleString('en-US', {
            style: 'currency',
            currency: 'USD'
        });
        cellSubtotal.textContent = parseFloat(subtotal).toLocaleString('en-US', {
            style: 'currency',
            currency: 'USD'
        });

        var deleteButton = document.createElement("button");
        deleteButton.innerText = "X";
        deleteButton.classList.add("deleteBtn", "bg-red-500", "text-white", "font-bold", "py-1", "px-2", "rounded");
        deleteButton.onclick = function() {
            newRow.remove();
            updateTotal();
        };
        cellActions.appendChild(deleteButton);

        updateTotal();

        document.getElementById("name").value = "";
        document.getElementById("cantidad").value = "";
        document.getElementById("price").value = "";
    }

    function updateTotal() {
        var total = 0;
        var rows = document.getElementById("myTableBody").rows;
        for (var i = 0; i < rows.length; i++) {
            total += parseFloat(rows[i].cells[3].textContent.replace(/[^0-9.-]+/g, ""));
        }
        document.getElementById("totalValue").textContent = `${parseFloat(total).toLocaleString('en-US', { style: 'currency', currency: 'USD' })}`;
    }

    function clearTable() {
        var tableBody = document.getElementById("myTableBody");
        tableBody.innerHTML = "";
        updateTotal();
    }
</script>

<x-form-section submit="updatePassword">
    <x-slot name="title">
        {{ __('Actualizar contraseña') }}
    </x-slot>

    <x-slot name="description">
        {{ __('Ingrese los datos necesarios.') }}
    </x-slot>

    {{-- <x-slot name="form">
        <div class="col-span-6 sm:col-span-4">
            <x-label for="current_password" value="{{ __('Contraseña Actual') }}" />
            <input id="current_password" type="password" placeholder="Contraseña Actual" class="input input-bordered w-full text-black bg-stone-50" required autocomplete="current-password"/>
        </div>

        <div class="col-span-6 sm:col-span-4">
            <x-label for="password" value="{{ __('Nueva Contraseña') }}" />
            <input id="password" type="password" name="password" placeholder="Nueva Contraseña" class="input input-bordered w-full text-black bg-stone-50" required autocomplete="new-password"/>
        </div>

        <div class="col-span-6 sm:col-span-4">
            <x-label for="password_confirmation" value="{{ __('Confirmar Nueva Contraseña') }}" />
            <input id="password_confirmation" type="password" name="password_confirmation" placeholder="Confirmar Nueva Contraseña" class="input input-bordered w-full text-black bg-stone-50" required autocomplete="new-password-confirmation"/>
        </div>
    </x-slot> --}}

    <x-slot name="form">
        <div class="col-span-6 sm:col-span-4">
            <x-label for="current_password" value="{{ __('Current Password') }}" />
            <input id="current_password" type="password"  wire:model="state.current_password" placeholder="Contraseña Actual" class="input input-bordered w-full text-black bg-stone-50" autocomplete="current-password" />
            <x-input-error for="current_password" class="mt-2" />
        </div>

        <div class="col-span-6 sm:col-span-4">
            <x-label for="password" value="{{ __('New Password') }}" />
            <input id="password" type="password"  wire:model="state.password" placeholder="Nueva Contraseña" class="input input-bordered w-full text-black bg-stone-50" autocomplete="new-password" />
            <x-input-error for="password" class="mt-2" />
        </div>

        <div class="col-span-6 sm:col-span-4">
            <x-label for="password_confirmation" value="{{ __('Confirm Password') }}" />
            <input id="password_confirmation" type="password" wire:model="state.password_confirmation" placeholder="Confirmar Nueva Contraseña" class="input input-bordered w-full text-black bg-stone-50" autocomplete="new-password" />
            <x-input-error for="password_confirmation" class="mt-2" />

        </div>
    </x-slot>



    <x-slot name="actions">
        <x-action-message class="me-3" on="saved">
            {{ __('Saved.') }}
        </x-action-message>

        <x-button>
            {{ __('Save') }}
        </x-button>
    </x-slot>
</x-form-section>

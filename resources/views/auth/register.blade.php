<x-guest-layout>
    <x-authentication-card>
        <x-slot name="logo">
            <h1 class="text-4xl font-bold">Register</h1>
        </x-slot>

        <x-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div>
                <x-label for="user_name" value="{{ __('Nombre de Usuario') }}"  class="mb-4"/>
                <input id="user_name" type="text" name="user_name" placeholder="Nombre" class="input input-bordered w-full text-black bg-stone-50" :value="old('user_name')" autofocus required autocomplete="user_name"/>
            </div>

            <div class="mt-4">
                <x-label for="user_paternal_surname" value="{{ __('Apellido Paterno') }}"  class="mb-4"/>
                <input id="user_paternal_surname" type="text" name="user_paternal_surname" placeholder="Apellido Paterno" class="input input-bordered w-full text-black bg-stone-50" autofocus required autocomplete="user_paternal_surname"/>
            </div>

            <div class="mt-4">
                <x-label for="user_maternal_surname" value="{{ __('Apellido Materno') }}"  class="mb-4"/>
                <input id="user_maternal_surname" type="text" name="user_maternal_surname" placeholder="Apellido Materno" class="input input-bordered w-full text-black bg-stone-50" autofocus required autocomplete="user_maternal_surname"/>
            </div>

            <div class="mt-4">
                <x-label for="department" value="{{ __('Seleccione un departamento:') }}"  class="mb-4"/>
                <select id="department" name="department" class="select select-bordered w-full max-w-xs text-black bg-stone-50">
                    <option disabled selected value="">Selecciona un departamento</option>
                    <option value="1" >Sistemas</option>
                    <option value="2" >Ventas</option>
                    <option value="3" >Marketing</option>
                    <option value="4" >Recursos Humanos</option>
                    <option value="5" >Producción</option>
                    <option value="6" >Finanzas</option>
                    <option value="7" >Logística</option>
                    <option value="8" >Calidad</option>
                    <option value="9" >Investigación y Desarrollo</option>
                    <option value="10" >Soporte Técnico</option>
                </select>
            </div>

            <div class="mt-4">
                <x-label for="email" value="{{ __('Email') }}" class="mb-4"/>
                <input id="email" type="email" name="email" placeholder="Email" class="input input-bordered w-full text-black bg-stone-50" :value="old('email')" required autocomplete="username"/>
            </div>

            <div class="mt-4">
                <x-label for="password" value="{{ __('Password') }}" class="mb-4" />
                <input id="password" type="password" name="password" placeholder="Password" class="input input-bordered w-full text-black bg-stone-50" required autocomplete="new-password"/>
            </div>

            <div class="mt-4">
                <x-label for="password_confirmation" value="{{ __('Confirm Password') }}" class="mb-4" />
                <input id="password_confirmation" type="password" name="password_confirmation" placeholder="Password Confirmation" class="input input-bordered w-full text-black bg-stone-50" required autocomplete="new-password"/>
            </div>


            @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
                <div class="mt-4">
                    <x-label for="terms">
                        <div class="flex items-center">
                            <x-checkbox name="terms" id="terms" required />

                            <div class="ms-2">
                                {!! __('I agree to the :terms_of_service and :privacy_policy', [
                                        'terms_of_service' => '<a target="_blank" href="'.route('terms.show').'" class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">'.__('Terms of Service').'</a>',
                                        'privacy_policy' => '<a target="_blank" href="'.route('policy.show').'" class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">'.__('Privacy Policy').'</a>',
                                ]) !!}
                            </div>
                        </div>
                    </x-label>
                </div>
            @endif

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-button class="ms-4">
                    {{ __('Register') }}
                </x-button>
            </div>
        </form>
    </x-authentication-card>
</x-guest-layout>

<div class="flex flex-col text-sm text-gray-700 font-bold bg-blue-200 px-3 py-3 rounded-lg shadow-sm">
    <div class="flex mr-1">
        <p class="mr-1">{{ 'Se Realizado el:' }}</p>
    </div>
    <div class="pl-1">
        <p class="flex text-xs font-light text-gray-700">{{"Dia: ". $date}}</p>
        <p class="flex text-xs font-light text-gray-700">{{"Hora: ". $time}}</p>
    </div>
</div>

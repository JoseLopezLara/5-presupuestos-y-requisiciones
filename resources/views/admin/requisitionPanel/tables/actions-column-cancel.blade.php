<div class="flex">
    <button
        x-data="{}" @click="$dispatch('open-modal', 'viewFullRequisitionAcceptedModal')"
        wire:click="$dispatch('loadRequisitionRequestCancel', { requisition_id: {{ $requisition_id }}})"
        class="bg-purple-500 hover:bg-purple-700 text-white font-bold py-1 px-5 rounded-md me-2 shadow-lg">
            <i class="fa fa-fw fa-retweet"></i>
    </button>
</div>

<div class="flex flex-col text-sm text-gray-700 font-bold bg-slate-200 px-3 py-3 rounded-lg shadow-sm">
    <div class="flex mr-1">
        <p class="mr-1">{{ 'Se divide en:' }}</p>
    </div>
    <div class="pl-1">
        <p class="flex text-xs font-light text-gray-700">{{"Total de conceptos ". $n_items}}</p>
        <p class="flex text-xs font-light text-gray-700">{{"Valor total: $". $total}}</p>
    </div>
</div>

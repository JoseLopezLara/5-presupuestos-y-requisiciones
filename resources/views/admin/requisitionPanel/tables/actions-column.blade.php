<div class="flex">
    <button
        x-data="{}" @click="$dispatch('open-modal', 'viewFullRequisitionRequestModal')"
        wire:click="$dispatch('loadRequisitionRequest', { requisition_id: {{ $requisition_id }}})"
        class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-5 rounded-md me-2 shadow-lg">
            <i class="fa fa-fw fa-eye"></i>
    </button>
</div>

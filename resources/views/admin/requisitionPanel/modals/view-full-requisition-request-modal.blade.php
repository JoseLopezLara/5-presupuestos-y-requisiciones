<div class="text-black">
    <x-modalL name="viewFullRequisitionRequestModal" x-data="{ openEvento: false }" x-show="openEvento" @open-modal.window="openEvento = true"
        @close-modal.window="openEvento = false" @refreshParent.window="openEvento = false" maxWidth="3xl" class=" ">

        <x-button x-data="{}" @click="$dispatch('close-modal', 'viewFullRequisitionRequestModal')"
            class="absolute top-0 right-0 mr-8 mt-4">
            <i class="fa fa-fw fa-times" style="color: #ffffff;"></i>
        </x-button>

        <h1 class="text-xl mt-5 font-bold text-center text-gray-700">DATOS DE LA REQUISICIÓN</h1>

        <hr class="border-t-2 border-gray-200 mt-3 mx-6">

        <div class="py-6 px-12">
            {{-- <div class="flex flex-col sm:flex-row items-center justify-center"> --}}
            <div class="flex pb-4 flex-col bg-gray-100 rounded-xl shadow">
                <div class=" bg-gray-200 rounded-t-xl py-4 shadow-md">
                    <p class="ml-6  text-lg font-bold">ID de la requisición: #{{ $requisition_id}}</p>

                    <div class="flex flex-col items-end justify-end mr-6 rounded-t-xl">
                        <div class="flex flex-row">
                            <p class="text-sm font-black">Requisición realizada el: </p>
                            <p class="pl-1 text-sm font-medium">{{ $create_date }}</p>
                            <p class="pl-1 text-sm font-black">a las:</p>
                            <p class="pl-1 text-sm font-medium">{{ $create_time }}</p>
                        </div>
                        <div class="flex flex-row">
                            <p class="text-sm font-black">Requisición actualizada el: </p>
                            <p class="pl-1 text-sm font-medium"> {{ $update_date }}</p>
                            <p class="pl-1 text-sm font-black">a las:</p>
                            <p class="pl-1 text-sm font-medium"> {{ $update_time }}</p>
                        </div>
                    </div>
                </div>



                <div class="flex mt-4 justify-end mr-8">
                    <div class="flex bg-emerald-200 ml-10 px-4 py-1 rounded-md">
                        <p class="text-xs font-bold">Departamento: </p>
                        <p class="pl-1 text-xs font-normal"> {{ $dept_name }}</p>
                    </div>
                </div>


                <div class="flex mx-8 mt-4">
                    <div class="flex flex-col bg-slate-200 px-12 py-4 rounded-md w-full">
                        <p class="mb-2 text-lg font-black text-center underline">Todos los Conceptos</p>
                        @if ($items !== null)
                            @foreach ($items as $index => $item)
                                <div class="flex flex-row mb-1">
                                    <p class="text-base font-black">Concepto: </p>
                                    <p class="pl-1 text-base font-medium"> {{ $item['item'] }}</p>
                                </div>
                                <div class="ml-4 flex flex-row items-center">
                                    <p class="text-sm font-black">Cantidad: </p>
                                    <input
                                        wire:model.live='items.{{ $index }}.quantity'
                                        wire:change="updateItemValue($event.target.value, {{ $index }})"
                                        class="bg-transparent  border-gray-300 rounded-sm text-sm ml-1 px-2 h-6 min-w-12 max-w-12"
                                        type="number"
                                        value="{{ $item['quantity'] }}">
                                </div>
                                <div class="ml-4 mt-2 flex flex-row items-center">
                                    <p class="text-sm font-black">Precio unitario: </p>
                                    <p class="pl-1 text-sm text-gray-600">$</p>
                                    <input
                                        wire:model.live='items.{{ $index }}.unit_price'
                                        wire:change="updateItemValue($event.target.value, {{ $index }})"
                                        class="bg-transparent border-gray-300 rounded-sm text-sm ml-0.5 px-2 h-6 min-w-16 max-w-16"
                                        type="text"
                                        value="{{ $item['unit_price'] }}">
                                </div>
                                <div class="mb-3 ml-4 mt-2 flex flex-row ">
                                    <p class="text-sm font-black">Subtotal: </p>
                                    <p class="pl-1 text-sm "> ${{ $item['subtotal'] }}</p>
                                </div>
                            @endforeach
                        @else
                            <!--TODO: EROR AL DECODIFICAR -->
                        @endif

                    </div>
                </div>

                <div class="flex my-4 mx-8">
                    <div class="flex flex-col bg-blue-200  pl-12 pr-12 py-4 rounded-md w-full">
                        <p class="mb-2 text-lg font-black text-center underline">Resumen</p>
                        <div class="flex flex-col">
                            <p class="text-base font-black">Total de la requisición: </p>
                            <p class="pl-3 text-base text"> ${{ $total }} </p>
                        </div>

                        <div class="flex flex-col">
                            <p class="text-base font-black">Presupuesto actual: </p>
                            <p class="pl-3 text-base text"> ${{ $dept_budget }} </p>
                        </div>

                        <div class="flex flex-col">
                            <p class="text-base font-black">Despues de aceptar: </p>
                            <div class="flex items-center">
                                <p class="pl-3 text-base font-bold text-red-700"> ${{ $dept_budget_if_accept_requisition }} </p>
                                @if ($budgetIsEnough === false)
                                    <p class="bg-red-200 w-max mx-12 ml-4  px-4 py-2 rounded-md font-bold text-xs">
                                        El presupuesto no es suficiente
                                    </p>
                                @endif
                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <!--VALIDATIONS-->

            @if ($errors->any())

                <ul class="bg-red-200 w-max mx-12 my-2 px-4 py-2 rounded-md">
                    @foreach ($errors->all() as $error)
                        <li class="font-bold text-xs">{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <!--END VALIDATIONS-->


            <div class="flex justify-end mx-4 mb-2 mt-8">
                @if ($budgetIsEnough)
                    <x-button
                        class="mx-2"
                        wire:click="$toggle('confirmAcceptRequisition')">
                            Aceptar requisición
                    </x-button>
                @else
                    <x-button
                        disabled
                        class="mx-2 opacity-40"
                        wire:click="$toggle('confirmAcceptRequisition')"
                        >
                            Aceptar requisición
                    </x-button>
                @endif


                <x-danger-button
                    class="mx-2"
                    wire:click="$toggle('confirmDenegateRequisition')">
                        Rechazar requisición
                </x-danger-button>
            </div>
    </x-modalL>

    <x-confirmation-modal wire:model="confirmAcceptRequisition" class="flex items-center justify-center mb-40">
        <x-slot name='title'>
            Aceptar Requisición
        </x-slot>

        <x-slot name='content'>
            ¿Estás seguro de aceptar la está requisición?
        </x-slot>

        <x-slot name='footer'>
            <x-button wire:click="performAcceptRequisition">Si, Aceptar</x-button>
            <x-danger-button
            class="mx-2"
            x-data="{}"
            @click="$dispatch('close-modal', 'viewFullRequisitionRequestModal')"
            wire:click="$toggle('confirmAcceptRequisition')">
                Cancelar
            </x-danger-button>
        </x-slot>
    </x-confirmation-modal>

    <x-confirmation-modal wire:model="confirmDenegateRequisition" class="flex items-center justify-center mb-40">
        <x-slot name='title'>
            Rechazar Requisición
        </x-slot>

        <x-slot name='content'>
            ¿Estás seguro de rechazar la está requisición?
        </x-slot>

        <x-slot name='footer'>
            <x-button wire:click="performDenegateRequisition">Si, Rechazar</x-button>
            <x-danger-button
            class="mx-2"
            x-data="{}"
            @click="$dispatch('close-modal', 'viewFullRequisitionRequestModal')"
            wire:click="$toggle('confirmDenegateRequisition')">
                Cancelar
            </x-danger-button>
        </x-slot>
    </x-confirmation-modal>



</div>

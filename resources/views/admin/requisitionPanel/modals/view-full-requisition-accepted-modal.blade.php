<div class="text-black">


    <x-modalL name="viewFullRequisitionAcceptedModal" x-data="{ openEvento: false }" x-show="openEvento" @open-modal.window="openEvento = true"
        @close-modal.window="openEvento = false" @refreshParent.window="openEvento = false" maxWidth="3xl" class="">

        <x-button x-data="{}" @click="$dispatch('close-modal', 'viewFullRequisitionAcceptedModal')"
            class="absolute top-0 right-0 m-2">
            <i class="fa fa-fw fa-times" style="color: #ffffff;"></i>
        </x-button>

        <h1 class="text-xl mt-5 font-bold text-center text-gray-700">RETORNAR REQUISICIÓN ACEPTADA</h1>

        <hr class="border-t-2 border-gray-200 mt-3 mx-6">

        <div class="pt-2 pb-1  px-12">
            {{-- <div class="flex flex-col sm:flex-row items-center justify-center"> --}}
            <div class="flex pb-4 flex-col bg-gray-100 rounded-xl shadow">
                <div class=" bg-gray-200 rounded-t-xl py-4 shadow-md">
                    <p class="ml-6  text-lg font-bold">ID de la requisición: #{{ $requisition_id}}</p>

                    <div class="flex flex-col items-end justify-end mr-6 rounded-t-xl">
                        <div class="flex flex-row">
                            <p class="text-sm font-black">Requisición realizada el: </p>
                            <p class="pl-1 text-sm font-medium">{{ $create_date }}</p>
                            <p class="pl-1 text-sm font-black">a las:</p>
                            <p class="pl-1 text-sm font-medium">{{ $create_time }}</p>
                        </div>
                        <div class="flex flex-row">
                            <p class="text-sm font-black">Requisición Aprobada el: </p>
                            <p class="pl-1 text-sm font-medium"> {{ $update_date }}</p>
                            <p class="pl-1 text-sm font-black">a las:</p>
                            <p class="pl-1 text-sm font-medium"> {{ $update_time }}</p>
                        </div>
                    </div>
                </div>



                <div class="flex mt-4 justify-end mr-8">
                    <div class="flex bg-emerald-200 ml-10 px-4 py-1 rounded-md">
                        <p class="text-xs font-bold">Departamento: </p>
                        <p class="pl-1 text-xs font-normal"> {{ $dept_name }}</p>
                    </div>
                </div>

                <div class="flex mx-8 mt-4">
                    <div class="flex flex-col bg-slate-200 px-12 py-4 rounded-md w-full">
                        <p class="mb-2 text-lg font-black text-center underline">Todos los Conceptos</p>
                        @if ($items !== null)
                            @foreach ($items as $item)
                                <div class="flex flex-row mb-1">
                                    <p class="text-base font-black">Concepto: </p>
                                    <p class="pl-1 text-base font-medium"> {{ $item['item'] }}</p>
                                </div>
                                <div class="ml-4 flex flex-row">
                                    <p class="text-sm font-black">Cantidad: </p>
                                    <p class="pl-1 text-sm "> {{ $item['quantity'] }} unidades</p>
                                </div>
                                <div class="ml-4 flex flex-row">
                                    <p class="text-sm font-black">Precio unitario: </p>
                                    <p class="pl-1 text-sm "> ${{ $item['unit_price'] }}</p>
                                </div>
                                <div class="mb-3 ml-4 flex flex-row ">
                                    <p class="text-sm font-black">Subtotal: </p>
                                    <p class="pl-1 text-sm "> ${{ $item['subtotal'] }}</p>
                                </div>
                            @endforeach
                        @else
                            <!--TODO: EROR AL DECODIFICAR -->
                        @endif

                    </div>
                </div>

                <div class="flex my-4 mx-8">
                    <div class="flex flex-col bg-blue-200  pl-12 pr-12 py-4 rounded-md w-full">
                        <p class="mb-2 text-lg font-black text-center underline">Resumen</p>
                        <div class="flex flex-col">
                            <p class="text-base font-black">Total de la requisición: </p>
                            <p class="pl-3 text-base text"> ${{ $total }} </p>
                        </div>

                        <div class="flex flex-col">
                            <p class="text-base font-black">Presupuesto actual: </p>
                            <p class="pl-3 text-base text"> ${{ $dept_budget }} </p>
                        </div>


                        <div class="flex flex-col">
                            <p class="text-base font-black">Despues de retorna: </p>
                            <p class="pl-3 text-base font-bold text-green-700"> ${{ $dept_budget_if_return_requisition }} </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="pt-3 pb-3 px-12">

            @if ($errors->any())

                <ul class="bg-red-200 w-max mx-12 my-2 px-4 py-2 rounded-md">
                    @foreach ($errors->all() as $error)
                        <li class="font-bold text-xs">{{ $error }}</li>
                    @endforeach
                </ul>
            @endif
            {{-- <div class="flex flex-col sm:flex-row items-center justify-center"> --}}
            <div class="flex pb-4 flex-col bg-gray-100 rounded-xl shadow">
                <p class="mx-6 my-1 text-base font-bold">Motivo de retorno:</p>
                <textarea
                    wire:model="motiveReturnRequisition"  rows="5"
                    class="rounded-xl mx-4 my-2 px-2 py-1 ">
                </textarea>
            </div>

            <div class="flex justify-end mx-4 mb-2 mt-8">
                <x-button
                    type="submit"
                    class="mx-2"
                    wire:click="$toggle('confirmReturnRequisition')">
                        RETORNAR REQUISICIÓN
                </x-button>
            </div>
        </div>
    </x-modalL>

    <x-confirmation-modal wire:model="confirmReturnRequisition" class="flex items-center justify-center mb-40">
        <x-slot name='title'>
            Retornar Requisición
        </x-slot>

        <x-slot name='content'>
            ¿Estás seguro de retornar la está requisición?
        </x-slot>

        <x-slot name='footer'>
            <x-button wire:click="performCreateReturnRequisition">Si, Retornar</x-button>
            <x-danger-button
            class="mx-2"
            x-data="{}"
            @click="$dispatch('close-modal', 'viewFullRequisitionAcceptedModal')"
            wire:click="$toggle('confirmReturnRequisition')">
                Cancelar
            </x-danger-button>
        </x-slot>
    </x-confirmation-modal>



</div>

<div class="bg-gray-200 bg-opacity-25 flex flex-col items-center md:grid md:grid-cols-2 p-6 lg:p-8">
    <form onsubmit="" method="POST">
        <div class="mb-4">
            <label for="opcion" class="block text-gray-700 text-sm font-bold mb-2">Departamento</label>
            <select id="opcion" name="opcion" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                <option value="opcion1">Sistemasssss</option>
                <option value="opcion2">Marketing</option>
                <option value="opcion3">Recursos Humanos</option>
            </select>
        </div>
        <div class="mb-4">
            <label for="presupuesto" class="block text-gray-700 text-sm font-bold mb-2">Presupuesto</label>
            <input type="number" id="presupuesto" name="presupuesto" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
        </div>
        <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Guardar</button>


    </form>
</div>
